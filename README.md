# ronto

Text editor written in D. Work in progress...

Made following a [tutorial](https://viewsourcecode.org/snaptoken/kilo/03.rawInputAndOutput.html).

## TODO
- [ ] If we wanted to support the maximum number of terminals out there, 
      we could use the ncurses library, which uses the terminfo database to figure out 
      the capabilities of a terminal and what escape sequences to use for that particular terminal.
      