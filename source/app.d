/*** includes ***/

import core.stdc.stdlib : atexit, exit;
import core.stdc.stdio : printf, perror, sscanf;
import core.stdc.ctype : iscntrl;
import core.stdc.errno : EAGAIN, errno;

import core.sys.posix.unistd;
import core.sys.posix.termios;
import core.sys.posix.sys.ioctl;

import std.stdio : stdout, dwrite = write, File;
import std.string : asCharArray = toStringz, rightJustify;
import std.algorithm.comparison : min;
import std.format : format;
import std.range : empty;
import std.conv : to;

const int controlKeyCode = 0b00011111;
const string rontoVersion = "0.0.1";

/*** defines ***/

T CTRL_KEY(T)(T key) { return ((key) & controlKeyCode); }

enum EditorKey : int {
	ARROW_LEFT = 1000,
	ARROW_RIGHT,
	ARROW_UP,
	ARROW_DOWN,
	DELETE,
	HOME,
	END,
	PAGE_UP,
	PAGE_DOWN
}

/*** data ***/

struct EditorConfig {
	int cursorRow;
	int cursorCol;
	int screenRows;
	int screenCols;
	int numRows;
	string editRow;
	termios orig_termios;
}
EditorConfig E;

/*** terminal ***/

void clearScreen() {
	write(STDOUT_FILENO, "\x1b[2J".asCharArray, 4);
	write(STDOUT_FILENO, "\x1b[H".asCharArray, 3);
}

void die(immutable string message) {
	clearScreen();	
	perror(message.asCharArray);
	exit(1);
}

extern (C) {
	void disableRawMode() {
		if (tcsetattr(STDIN_FILENO, TCSANOW, &E.orig_termios) == -1) {
			die("tcsetattr");
		}
	}
}

void enableRawMode() {
	if (tcgetattr(STDIN_FILENO, &E.orig_termios) == -1) {
		die("tcgetattr");
	}
	
	atexit(&disableRawMode);

	termios raw = E.orig_termios;
	raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
	raw.c_oflag &= ~(OPOST);
	raw.c_cflag |= (CS8);
	raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
	raw.c_cc[VMIN] = 0;
	raw.c_cc[VTIME] = 1;

	tcsetattr(STDIN_FILENO, TCSANOW, &raw);
}

int editorReadKey() {
	long nread;
	char c;

	while ((nread = read(STDIN_FILENO, &c, 1)) != 1) {
		if (nread == -1 && errno != EAGAIN) {
			die("read");
		}
	}

	if (c == '\x1b') {
		char[3] seq;

		if (read(STDIN_FILENO, &seq[0], 1) != 1) return '\x1b';
		if (read(STDIN_FILENO, &seq[1], 1) != 1) return '\x1b';

		if (seq[0] == '[') {
			if (seq[1] >= '0' && seq[1] <= '9') {
				if (read(STDIN_FILENO, &seq[2], 1) != 1) return '\x1b';
				if (seq[2] == '~') {
					switch (seq[1]) {
						case '3': return EditorKey.DELETE;
						case '1':
						case '7': return EditorKey.HOME;
						case '2':
						case '8': return EditorKey.END;
						case '5': return EditorKey.PAGE_UP;
						case '6': return EditorKey.PAGE_DOWN;
						default:
							break;
					}
				}
			} else {
				switch (seq[1]) {
					case 'A': return EditorKey.ARROW_UP;
					case 'B': return EditorKey.ARROW_DOWN;
					case 'C': return EditorKey.ARROW_RIGHT;
					case 'D': return EditorKey.ARROW_LEFT;
					case 'H': return EditorKey.HOME;
					case 'F': return EditorKey.END;
					default:
						break;
				}
			}
		} else if (seq[0] == 'O') {
			switch (seq[1]) {
				case 'H': return EditorKey.HOME;
				case 'F': return EditorKey.END;
				default:
					break;
			}
		}

		return '\x1b';
	} 

	return c;
}

int getCursorPosition(ref int rows, ref int cols) {
	char[32] buf;
	uint i;
	
	if (write(STDOUT_FILENO, "\x1b[6n".asCharArray, 4) != 4) return -1;

	for (i = 0; i != buf.length; ++i) {
		if (read(STDIN_FILENO, &buf[i], 1) != 1) break;
		if (buf[i] == 'R') break;
	}
	buf[i] = '\0';

	if (buf[0] != '\x1b' || buf[1] != '[') return -1;
	if (sscanf(&buf[2], "%d;%d", &rows, &cols) != 2) return -1;
	
	return 0;
}

int getWindowSize(ref int rows, ref int cols) {
	winsize ws;

	if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0) {
		if (write(STDOUT_FILENO, "\x1b[999C\x1b[999B".asCharArray, 12) != 12) return -1;
		return getCursorPosition(rows, cols);
	} else {
		cols = ws.ws_col;
		rows = ws.ws_row;
		return 0;
	}
}

/*** file i/o ***/

void editorOpen(string filename) {
	auto file = File(filename);

	foreach (line; file.byLine()) {
		if (! line.empty) {		
			E.editRow = line.to!string;
			E.numRows = 1;
			break;
		}
	}
}

/*** append buffer ***/
// We will use a simple string

/*** output ***/

void editorDrawRows(ref string buffer) {
	foreach (int y; 0 .. E.screenRows) {
		if ( y >= E.numRows ) {
			if (E.numRows == 0 && y == E.screenRows / 3) {
				string welcome = format("Ronto editor -- version %s", rontoVersion);
				welcome = welcome[0..min($, E.screenCols)];
			
				long padding = (E.screenCols - welcome.length) / 2;
				if (padding) {
					buffer ~= "~";
					padding--;
				}

				buffer ~= welcome.rightJustify(padding + welcome.length, ' ');
			} else {
				buffer ~= "~";
			}
		} else {
			buffer ~= E.editRow[0..min($, E.screenCols)];
		}
		
		buffer ~= "\x1b[K";
		if (y < E.screenRows - 1) {
			buffer ~= "\r\n";
		}
	}
}

void editorRefreshScreen() {
	string buffer;

	buffer ~= "\x1b[?25l";
	buffer ~= "\x1b[H";

	editorDrawRows(buffer);

	buffer ~= "\x1b[%d;%dH".format(E.cursorRow + 1, E.cursorCol + 1);

	buffer ~= "\x1b[?25h";
	// TODO: Why isn't stdout.write(buffer) not working
	// the cursor is not drawn at the top left of the screen
	// stdout.write(buffer);
	write(STDOUT_FILENO, buffer.asCharArray, buffer.length);
}

/*** input ***/

void editorMoveCursor(int key) {
	switch (key) {
		case EditorKey.ARROW_LEFT:
			if (E.cursorCol != 0) {
				E.cursorCol--;
			}
			break;
		case EditorKey.ARROW_RIGHT:
			if (E.cursorCol != E.screenCols - 1)  {
				E.cursorCol++;
			}
			break;
		case EditorKey.ARROW_UP:
			if (E.cursorRow != 0) {
				E.cursorRow--;
			}
			break;
		case EditorKey.ARROW_DOWN:
			if (E.cursorRow != E.screenRows - 1){
				E.cursorRow++;
			}
			break;
		default:
			break;
	}
}

void editorProcessKeypress() {
	int c = editorReadKey();

	switch (c) {
		case CTRL_KEY('q'):
			clearScreen();
			exit(0);

		case EditorKey.HOME:
			E.cursorCol = 0;
			break;

		case EditorKey.END:
			E.cursorCol = E.screenCols - 1;
			break;
		
		case EditorKey.PAGE_UP:
		case EditorKey.PAGE_DOWN:
			foreach (time; 0..E.screenRows)
				editorMoveCursor(c == EditorKey.PAGE_UP ? EditorKey.ARROW_UP : EditorKey.ARROW_DOWN);
			break;
		case EditorKey.ARROW_LEFT:
		case EditorKey.ARROW_RIGHT:
		case EditorKey.ARROW_UP:
		case EditorKey.ARROW_DOWN:
			editorMoveCursor(c);
			break;
		default:
			break;
	}
}

/*** init ***/

void initEditor() {
	E.cursorRow = 0;
	E.cursorCol = 0;
	E.numRows = 0;
	if (getWindowSize(E.screenRows, E.screenCols) == -1) {
		die("getWindowSize");
	}
}

void main(string[] args)
{
	enableRawMode();
	initEditor();
	if (args.length >= 2) {
		editorOpen(args[1]);
	}

	while (true) {
		editorRefreshScreen();
		editorProcessKeypress();
	}
}
